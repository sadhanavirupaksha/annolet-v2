annoletContainer();

// Function to create container for annolet
function annoletContainer() {
    var body = document.getElementsByTagName('body')[0];
    var head = document.getElementsByTagName('head')[0];
    // Inject jquery in a webpage
    var jquery_script = document.createElement('script');
    jquery_script.type = 'text/javascript';
    jquery_script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'; 
    body.appendChild(jquery_script);
    // Inject the jQuery DD Icon Menu plugin in a webpage
    var menuplugin_csstag = document.createElement('link');
    menuplugin_csstag.href = 'https://gl.githack.com/sadhanavirupaksha/annolet-v2/raw/master/src/jQuery-Plugin-For-Animated-Side-Icon-Menu-DD-Icon-Menu/iconmenu.css';
    menuplugin_csstag.rel = 'stylesheet';
    head.appendChild(menuplugin_csstag);
    var menuplugin_script = document.createElement('script');
    menuplugin_script.type = 'text/javascript';
    menuplugin_script.src = 'https://gl.githack.com/sadhanavirupaksha/annolet-v2/raw/master/src/jQuery-Plugin-For-Animated-Side-Icon-Menu-DD-Icon-Menu/iconmenu.js'; 
    body.appendChild(menuplugin_script);  
    //appending a div(annolet container) to body element of a webpage.
    var container = document.createElement('div');
    container.id = 'sticky-container';
    body.appendChild(container);
    //injecting html code
    container.innerHTML =   '<ul id="myiconmenu" class="iconmenu">'+
                                '<li><a class="icon-list-ul" href="#" rel="csslibrary"></a></li>'+
                                '<li><a class="icon-search" href="#" rel="ddcontentarea"></a></li>'+
                                '<li><a class="icon-gears" href="#" rel="webtools" title="Web Tools"></a></li>'+
                                '<li><a class="icon-rss" href="#" rel="[title]" title="RSS"></a></li>'+
                                '<li><a class="icon-twitter" href="#" rel="[title]" title="Twitter"></a></li>'+
                            '</ul>'+
                            //CSS Library sub menu 
                            '<div id="csslibrary" class="iconsubmenu dropdownmenu">'+
                                '<ul class="ulmenu">'+
                                    '<li><a href="#" rel="webtools">Web Tools</a></li>'+
                                    '<li><a href="#">Horizontal CSS Menus</a></li>'+
                                    '<li><a href="#">Vertical CSS Menus</a></li>'+
                                    '<li><a href="#">Image CSS</a></li>'+
                                    '<li><a href="#">Form CSS</a></li>'+
                                    '<li><a href="#">DIVs and containers</a></li>'+
                                    '<li><a href="#">Links & Buttons</a></li>'+
                                    '<li><a href="#">CSS3 demos</a></li>'+
                                '</ul>'+
                            '</div>'+
                            //General Content sub menu 
                            '<div id="ddcontentarea" class="iconsubmenu mixedcontent">'+
                                '<p style="margin:5px 0 10px 0"><b>Visit the following main content areas of Dynamic Drive:</b></p>'+
                                '<div class="column">'+
                                    '<ul>'+
                                        '<li><a href="#">DHTML Scripts</a></li>'+
                                        '<li><a href="#">Whats New</a></li>'+
                                        '<li><a href="#">Revised</a></li>'+
                                    '</ul>'+
                                '</div>'+
                                '<div class="column">'+
                                    '<ul>'+
                                        '<li><a href="#" rel="csslibrary">CSS Library</a></li>'+
                                        '<li><a href="#">DHTML Forums</a></li>'+
                                        '<li><a href="#" rel="webtools">Web Tools</a></li>'+
                                    '</ul>'
                                '</div>'
                            '</div>'
                            //Web Tools sub menu 
                            '<div id="webtools" class="iconsubmenu dropdownmenu">'+
                                '<ul class="ulmenu">'
                                    '<li><a href="#">Image Optimizer</a></li>'+
                                    '<li><a href="#">FavIcon Generator</a></li>'+
                                    '<li><a href="#">Animated Gif Generator</a></li>'+
                                    '<li><a href="#">Email Riddler</a></li>'+
                                    '<li><a href="#">.htaccess Password Generator</a></li>'+
                                    '<li><a href="#">Gradient Image Maker</a></li>'+
                                    '<li><a href="#">.htaccess Banning Generator</a></li>'+
                                    '<li><a href="#">Button Maker</a></li>'+
                                    '<li><a href="#">Ribbon Rules Generator</a></li>'+
                               '</ul>'+
                            '</div>';
                            '<script>'+
                                'ddiconmenu.docinit({'+ // initialize an Icon Menu
                                    "menuid:'myiconmenu',"+ //main menu ID
                                    'easing:"easeInOutCirc",'+
                                    'dur:500})' + //<--no comma after last setting
                            '</script>';
}
